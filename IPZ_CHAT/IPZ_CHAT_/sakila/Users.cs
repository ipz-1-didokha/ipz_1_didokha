﻿using System;
using System.Collections.Generic;

namespace IPZ_CHAT_KOVALENKO.sakila
{
    public partial class Users
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
    }
}
